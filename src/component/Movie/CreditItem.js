import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {commonStyles} from '../../common';
import {RFValue} from 'react-native-responsive-fontsize';
import Api from '../../server/Api';

const CreditItem = ({item}) => {
  return (
    <View style={styles.item}>
      <Image
        style={styles.poster}
        source={{uri: Api.media + item.profile_path}}
      />
      <Text numberOfLines={1} style={styles.name}>
        {item.name}
      </Text>
    </View>
  );
};
export default CreditItem;

const styles = StyleSheet.create({
  item: {
    width: commonStyles.screenWidth / 3.5,
    height: commonStyles.screenWidth / 3.5,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 15,
    alignItems: 'center',
    marginLeft: RFValue(5, commonStyles.screenHeight),
  },
  poster: {
    width: RFValue(80, commonStyles.screenHeight),
    height: RFValue(80, commonStyles.screenHeight),
    borderRadius: RFValue(50, commonStyles.screenHeight),
    alignSelf: 'center',
    // resizeMode: '',
    marginTop: 10,
  },
  name: {
    fontSize: RFValue(16, commonStyles.screenHeight),
    fontWeight: '600',
    alignSelf: 'center',
    marginTop: RFValue(15, commonStyles.screenHeight),
  },
});
