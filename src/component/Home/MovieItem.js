import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {commonStyles, colors} from '../../common';
import {RFValue} from 'react-native-responsive-fontsize';
import Api from '../../server/Api';
import CategoryButton from '../button/CategoryButton';

const MovieItem = ({item, genre, navigation}) => {
  // get genre names relted to movie max 3

  item.genre_ids.length = item.genre_ids.length > 3 ? 3 : item.genre_ids.length;
  let genreList = genre
    .filter((x) => item.genre_ids.includes(x.id))
    .map((x) => x);
  genreList.length = 3;

  return (
    <TouchableOpacity
      activeOpacity={0.5}
      style={styles.item}
      onPress={() => navigation.navigate('Details', {id: item.id})}>
      <View style={styles.content}>
        <Image
          style={styles.poster}
          source={{uri: Api.media + item.backdrop_path}}
        />
        <View style={styles.data}>
          <Text style={styles.title} numberOfLines={2}>
            {item.title}
          </Text>
          <Text style={styles.date}>{item.release_date}</Text>
          <View style={commonStyles.categs_container}>
            {genreList.map((type) => (
              <CategoryButton key={type.id} text={type.name} />
            ))}
          </View>
        </View>
        <Text style={styles.percintage}>{item.vote_average * 10} %</Text>
      </View>
    </TouchableOpacity>
  );
};
export default MovieItem;

const styles = StyleSheet.create({
  item: {
    width: commonStyles.screenWidth / 1.1,
    paddingVertical: RFValue(10, commonStyles.screenHeight),
    alignSelf: 'center',
    justifyContent: 'center',
    padding: 10,
    backgroundColor: colors.white,
    borderRadius: 8,
    marginTop: 15,
    elevation: 5,
    shadowColor: 'grey',
    shadowOffset: {
      width: 3,
      height: 5,
    },
    shadowOpacity: 0.4,
    shadowRadius: 3.84,
  },
  poster: {
    width: RFValue(90, commonStyles.screenHeight),
    height: RFValue(150, commonStyles.screenHeight),
    borderRadius: RFValue(10, commonStyles.screenHeight),
    alignSelf: 'center',
  },
  content: {flexDirection: 'row'},
  data: {marginLeft: RFValue(10, commonStyles.screenHeight), width: '50%'},
  title: {
    fontSize: RFValue(17, commonStyles.screenHeight),
    fontWeight: 'bold',
    marginLeft: RFValue(7, commonStyles.screenHeight),
  },
  date: {
    marginTop: RFValue(10, commonStyles.screenHeight),
    fontSize: RFValue(17, commonStyles.screenHeight),
    color: colors.darkGrey,
    fontWeight: 'bold',
    marginLeft: RFValue(7, commonStyles.screenHeight),
  },
  percintage: {
    position: 'absolute',
    right: RFValue(10, commonStyles.screenHeight),
    bottom: RFValue(10, commonStyles.screenHeight),
    color: colors.green,
    fontWeight: 'bold',
    fontSize: RFValue(20, commonStyles.screenHeight),
  },
});
