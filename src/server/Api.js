export default {
  base_url: 'https://api.themoviedb.org/3',
  media: 'https://image.tmdb.org/t/p/w500/',
  topRated: '/movie/top_rated',
  popular: '/movie/popular',
  upcoming: '/movie/upcoming',
  genre: '/genre/movie/list',
  movie_details: '/movie/',
};
