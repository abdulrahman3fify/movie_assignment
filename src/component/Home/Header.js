import React from 'react';
import {View} from 'react-native';
import styles from '../../common/styles';

const Header = (props) => {
  return <View style={styles.header}>{props.children}</View>;
};
export default Header;
