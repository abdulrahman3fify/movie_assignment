import axios from 'axios';
import Api from './Api';
import {api_key} from './config';
export const common = () => {
  const getRequest = (path) => {
    return new Promise((resolve, reject) => {
      axios
        .get(`${Api.base_url}${path}?api_key=` + api_key)
        .then((res) => {
          return res.data;
        })
        .then((respnseJson) => resolve(respnseJson))
        .catch((error) => {
          reject(error);
        });
    });
  };

  return {getRequest};
};
