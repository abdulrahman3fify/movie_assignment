import React, {useEffect, useRef, useState} from 'react';
import {Text, StatusBar, StyleSheet, FlatList, View} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import useColorScheme from 'react-native/Libraries/Utilities/useColorScheme';
import {commonStyles} from '../../common';
import CustomButton from '../../component/button/CustomButton';
import {Header, HeaderTaps, MovieItem} from '../../component/Home';
import Api from '../../server/Api';
import {common} from '../../server/common';

const {getRequest} = common();

const Home = ({navigation}) => {
  const flatListRef = useRef(null);
  const isDarkMode = useColorScheme() === 'dark';
  const tabs = ['Upcoming', 'Popular', 'Top Rated'];
  const [active, setActive] = useState(0);
  const [upcoming, setUpcoming] = useState([]);
  const [popular, setPopular] = useState([]);
  const [top_rated, setTop_rated] = useState([]);
  const [genre, setGenre] = useState([]);

  useEffect(() => {
    //  getUpcoming movies
    getRequest(Api.upcoming).then((res) => {
      setUpcoming(res.results);
    });
    // getPopular movies
    getRequest(Api.popular).then((res) => {
      console.log(res, 'getPopular');
      setPopular(res.results);
    });
    // getTopRated movies
    getRequest(Api.topRated).then((res) => {
      console.log(res, 'getTopRated');
      setTop_rated(res.results);
    });
    // getGenre for movie types
    getRequest(Api.genre).then((res) => {
      console.log(res.genres, 'genre');
      setGenre(res.genres);
    });
  }, []);
  // handle tab press
  const handleTabChange = (index) => {
    setActive(index);
    flatListRef.current.scrollToOffset({animated: true, offset: 0});
  };

  return (
    <View style={commonStyles.container}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <Header>
        <Text style={styles.header_title}>Movies</Text>
      </Header>
      <HeaderTaps>
        {tabs.map((item, index) => {
          return (
            <CustomButton
              onPress={() => handleTabChange(index)}
              active={active === index}
              key={item}
              text={item}
            />
          );
        })}
      </HeaderTaps>
      <FlatList
        contentContainerStyle={styles.content}
        ref={flatListRef}
        key={active}
        getItemLayout={(data, index) => ({
          length: 100,
          offset: 100 * index,
          index,
        })}
        data={active === 0 ? upcoming : active === 1 ? popular : top_rated}
        renderItem={({item, index}) => (
          <MovieItem
            navigation={navigation}
            index={index}
            genre={genre}
            key={item.id}
            item={item}
          />
        )}
        keyExtractor={(item) => item.id}
        scrollsToTop={true}
      />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  header_title: {
    fontWeight: 'bold',
    margin: RFValue(15, commonStyles.screenHeight),
    fontSize: RFValue(30, commonStyles.screenHeight),
  },
  content: {
    paddingBottom: RFValue(50, commonStyles.screenHeight),
    paddingTop: RFValue(20, commonStyles.screenHeight),
  },
});
