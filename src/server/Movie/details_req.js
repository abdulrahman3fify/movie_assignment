import axios from 'axios';
import Api from '../Api';
import {api_key} from '../config';
export const details_req = () => {
  const getMovie = (id) => {
    return new Promise((resolve, reject) => {
      axios
        .get(`${Api.base_url}${Api.movie_details}${id}?api_key=` + api_key)
        .then((res) => {
          return res.data;
        })
        .then((respnseJson) => resolve(respnseJson))
        .catch((error) => {
          reject(error);
        });
    });
  };

  return {getMovie};
};
