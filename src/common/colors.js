export default {
  green: '#01B95D',
  grey: '#D0D0D0',
  darkGrey: 'grey',
  white: 'white',
  black: 'black',
};
