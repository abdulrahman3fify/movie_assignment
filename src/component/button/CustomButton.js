import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {commonStyles, colors} from '../../common';
import {RFValue} from 'react-native-responsive-fontsize';

const CustomButton = ({text, active, onPress}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.7}
      style={[styles.button, isButtonActive(active)]}>
      <Text
        style={[styles.text, {color: active ? colors.white : colors.black}]}>
        {text}
      </Text>
    </TouchableOpacity>
  );
};
export default CustomButton;
const isButtonActive = (active) => {
  if (active) {
    return {
      elevation: 5,
      backgroundColor: colors.green,
      shadowColor: 'grey',
      shadowOffset: {
        width: 3,
        height: 5,
      },
      shadowOpacity: 0.4,
      shadowRadius: 3.84,
    };
  }
};
const styles = StyleSheet.create({
  button: {
    width: '30%',
    alignItems: 'center',
    justifyContent: 'center',
    height: RFValue(40, commonStyles.screenHeight),
    borderRadius: 20,
    backgroundColor: colors.grey,
  },
  text: {
    fontWeight: '700',
    fontSize: RFValue(14, commonStyles.screenHeight),
  },
});
