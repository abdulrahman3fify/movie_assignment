import Header from './Header';
import MovieItem from './MovieItem';
import HeaderTaps from './HeaderTaps';
export {HeaderTaps, Header, MovieItem};
