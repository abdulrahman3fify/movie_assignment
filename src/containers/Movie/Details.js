import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import useColorScheme from 'react-native/Libraries/Utilities/useColorScheme';
import {colors, commonStyles} from '../../common';
import Api from '../../server/Api';
import {details_req} from '../../server/Movie/details_req';
import {common} from '../../server/common';
import CategoryButton from '../../component/button/CategoryButton';
import CreditItem from '../../component/Movie/CreditItem';

const {getMovie} = details_req();
const {getRequest} = common();
const Details = ({route}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const [movie, setMovie] = useState([]);
  const [credit, setCredit] = useState([]);
  const {id} = route.params;
  useEffect(() => {
    //  get movie details
    getMovie(id).then((res) => {
      console.log(id, res);
      setMovie(res);
    });
    getRequest(`/movie/${id}/credits`).then((res) => {
      console.log(id, res);
      setCredit(res.cast);
    });
  }, [id]);
  return (
    <View style={commonStyles.container}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView>
        <View style={styles.poster_container}>
          <Image
            style={styles.image}
            source={{uri: Api.media + movie.poster_path}}
          />
        </View>
        <Text style={styles.name} numberOfLines={2}>
          {movie.title}
        </Text>
        {movie.vote_average && (
          <Text style={styles.percintage}>{movie.vote_average * 10} %</Text>
        )}
        <View style={commonStyles.content}>
          <Text style={styles.title}>Overview</Text>
          <Text style={styles.overview}>{movie.overview}</Text>
          <Text style={styles.title}>Genre</Text>
          <View style={commonStyles.categs_container}>
            {movie?.genres?.map((type) => (
              <CategoryButton key={type.id} text={type.name} />
            ))}
          </View>
          <Text style={styles.title}>Credits</Text>
        </View>
        <FlatList
          contentContainerStyle={styles.flatList}
          horizontal
          data={credit}
          renderItem={({item}) => <CreditItem key={item.id} item={item} />}
          scrollsToTop={true}
        />
      </ScrollView>
    </View>
  );
};
export default Details;

const styles = StyleSheet.create({
  poster_container: {
    width: '100%',
    height: commonStyles.screenHeight / 3,
    marginTop: RFValue(15, commonStyles.screenHeight),
  },
  image: {
    width: '50%',
    height: '100%',
    alignSelf: 'center',
    borderRadius: 10,
  },
  name: {
    fontSize: RFValue(25, commonStyles.screenHeight),
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: RFValue(15, commonStyles.screenHeight),
    textAlign: 'center',
  },
  percintage: {
    color: colors.green,
    fontWeight: 'bold',
    marginTop: RFValue(15, commonStyles.screenHeight),
    fontSize: RFValue(22, commonStyles.screenHeight),
    alignSelf: 'center',
  },
  title: {
    fontSize: RFValue(20, commonStyles.screenHeight),
    fontWeight: 'bold',
    marginTop: RFValue(15, commonStyles.screenHeight),
  },
  overview: {
    fontSize: RFValue(17, commonStyles.screenHeight),
    marginTop: RFValue(10, commonStyles.screenHeight),
    lineHeight: 20,
  },
  flatList: {paddingLeft: 10, paddingRight: 10, paddingBottom: 35},
});
