import React from 'react';
import {Text, View} from 'react-native';
import {commonStyles, colors} from '../../common';

const CategoryButton = ({text, active}) => {
  return (
    <View style={commonStyles.button}>
      <Text
        style={[
          commonStyles.text,
          {color: active ? colors.white : colors.black},
        ]}>
        {text}
      </Text>
    </View>
  );
};
export default CategoryButton;
