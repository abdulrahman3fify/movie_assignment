import React from 'react';
import {StyleSheet, View} from 'react-native';
import commonStyles from '../../common/styles';

const HeaderTaps = (props) => {
  return <View style={styles.container}>{props.children}</View>;
};
export default HeaderTaps;

const styles = StyleSheet.create({
  container: {
    width: commonStyles.screenWidth / 1.1,
    alignSelf: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 10,
  },
});
