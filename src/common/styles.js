import {Dimensions, StyleSheet} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {getStatusBarHeight} from 'react-native-status-bar-height';
import colors from './colors';
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default StyleSheet.create({
  screenHeight: screenHeight,
  screenWidth: screenWidth,
  header: {marginTop: getStatusBarHeight()},
  container: {flex: 1, backgroundColor: colors.white},
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    height: RFValue(30, screenHeight),
    borderRadius: 20,
    backgroundColor: colors.grey,
    paddingHorizontal: RFValue(10, screenHeight),
    marginTop: RFValue(10, screenHeight),
    marginLeft: RFValue(7, screenHeight),
  },
  text: {
    fontWeight: '700',
    fontSize: RFValue(14, screenHeight),
  },
  content: {width: screenWidth / 1.1, alignSelf: 'center'},
  categs_container: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 5,
    marginLeft: -7,
  },
});
